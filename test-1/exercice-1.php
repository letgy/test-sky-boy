<?php

// Script need to be executed with cli

// Initialize variables to loop
$start = 1;
$end = 100;

// Get numbers from $start to $end
for ($number = $start; $number <= $end; $number++) {

    if ($number % 3 === 0 && $number % 5 === 0) {
        // Number is a multiple of 5 and 3
        echo 'FizzBuzz' . "\n";

    } else if ($number % 5 === 0) {
        // Number is a multiple of 5
        echo 'Buzz' . "\n";

    } else if ($number % 3 === 0) {
        // Number is a multiple of 3
        echo 'Fizz' . "\n";

    } else {
        // Other number
        echo (string)$number;
        echo "\n";
    }
}
