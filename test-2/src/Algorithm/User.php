<?php

namespace Eurecab\Finder\Algorithm;

use DateTime;
use Exception;

final class User
{
    /** @var string */
    private $name;

    /** @var DateTime */
    private $birthDate;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getBirthDate(): DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime|string $birthDate
     * @throws Exception
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = is_string($birthDate) ? new DateTime($birthDate) : $birthDate;
    }
}
