<?php

namespace Eurecab\Finder\Algorithm;

final class UsersBirthDays
{
    /** @var User[] */
    private $_users;

    /** @var UsersDifference[] */
    private $_usersDifferences = [];

    public function __construct(array $users)
    {
        $this->_users = $users;

        // Check if users are at least two
        if (sizeof($this->_users) >= 2) {
            // Get users differences
            for ($indexFirstUser = 0; $indexFirstUser < sizeof($this->_users); $indexFirstUser++) {
                for ($indexSecondUser = $indexFirstUser + 1; $indexSecondUser < sizeof($this->_users); $indexSecondUser++) {
                    // Add users differences
                    $this->_usersDifferences[] = new UsersDifference($this->_users[ $indexFirstUser ], $this->_users[ $indexSecondUser ]);
                }
            }
        }
    }

    public function getClosest(): UsersDifference
    {
        // Check if users differences available
        if (!empty($this->_usersDifferences)) {
            // Get first users difference as default
            $userDifference = $this->_usersDifferences[0];
            foreach ($this->_usersDifferences as $row) {
                // Check if the current user difference is closest
                if ($row->getDifferenceTime() < $userDifference->getDifferenceTime()) {
                    $userDifference = $row;
                }
            }

        } else {
            $userDifference = new UsersDifference();
        }

        return $userDifference;
    }

    public function getFurthest(): UsersDifference
    {
        // Check if users differences available
        if (!empty($this->_usersDifferences)) {
            // Get first users difference as default
            $userDifference = $this->_usersDifferences[0];
            foreach ($this->_usersDifferences as $row) {
                // Check if the current user difference is furthest
                if ($row->getDifferenceTime() > $userDifference->getDifferenceTime()) {
                    $userDifference = $row;
                }
            }

        } else {
            $userDifference = new UsersDifference();
        }

        return $userDifference;
    }

}
