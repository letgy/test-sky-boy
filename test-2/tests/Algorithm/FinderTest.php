<?php

namespace Eurecab\FinderTest\Algorithm;

use Eurecab\Finder\Algorithm\UsersBirthDays;
use Eurecab\Finder\Algorithm\User;
use Exception;
use PHPUnit\Framework\TestCase;

final class FinderTest extends TestCase
{

    /** @var User */
    private $sue;

    /** @var User */
    private $greg;

    /** @var User */
    private $sarah;

    /** @var User */
    private $mike;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->sue = new User();
        $this->sue->setName('Sue');
        $this->sue->setBirthDate('1950-01-01');

        $this->greg = new User();
        $this->greg->setName('Greg');
        $this->greg->setBirthDate('1952-05-01');

        $this->sarah = new User();
        $this->sarah->setName('Sarah');
        $this->sarah->setBirthDate('1982-01-01');

        $this->mike = new User();
        $this->mike->setName('Mike');
        $this->mike->setBirthDate('1979-01-01');
    }

    /** @test */
    public function should_return_empty_when_given_empty_list()
    {
        $users = [];

        $birthDays = new UsersBirthDays($users);
        $closest = $birthDays->getClosest();

        $this->assertEquals(null, $closest->getYoungest());
        $this->assertEquals(null, $closest->getOldest());
    }

    /** @test */
    public function should_return_empty_when_given_one_person()
    {
        $users = [
            $this->sue
        ];

        $birthDays = new UsersBirthDays($users);
        $closest = $birthDays->getClosest();

        $this->assertEquals(null, $closest->getYoungest());
        $this->assertEquals(null, $closest->getOldest());
    }

    /** @test */
    public function should_return_closest_two_for_two_people()
    {
        $users = [
            $this->sue,
            $this->greg
        ];

        $birthDays = new UsersBirthDays($users);
        $closest = $birthDays->getClosest();

        $this->assertEquals($this->sue, $closest->getYoungest());
        $this->assertEquals($this->greg, $closest->getOldest());
    }

    /** @test */
    public function should_return_furthest_two_for_two_people()
    {
        $users = [
            $this->mike,
            $this->greg
        ];

        $birthDays = new UsersBirthDays($users);
        $furthest = $birthDays->getFurthest();

        $this->assertEquals($this->greg, $furthest->getYoungest());
        $this->assertEquals($this->mike, $furthest->getOldest());
    }

    /** @test */
    public function should_return_furthest_two_for_four_people()
    {
        $users = [
            $this->sue,
            $this->sarah,
            $this->mike,
            $this->greg
        ];

        $birthDays = new UsersBirthDays($users);
        $furthest = $birthDays->getFurthest();

        $this->assertEquals($this->sue, $furthest->getYoungest());
        $this->assertEquals($this->sarah, $furthest->getOldest());
    }

    /** @test */
    public function should_return_closest_two_for_four_people()
    {
        $users = [
            $this->sue,
            $this->sarah,
            $this->mike,
            $this->greg
        ];

        $birthDays = new UsersBirthDays($users);
        $closest = $birthDays->getClosest();

        $this->assertEquals($this->sue, $closest->getYoungest());
        $this->assertEquals($this->greg, $closest->getOldest());
    }
}
