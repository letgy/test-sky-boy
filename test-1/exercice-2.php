<?php

// Script need to be executed with cli

// Get current position of a value in an array
function valuePosition(array $arr, int $number): int
{
    // Get position from array search
    $position = array_search($number, $arr);

    // If no value found, return -1 and not false
    return ($position !== false) ? $position : -1;
}

// Array of int
$arr = [4, 5, 8, 15, 21, 24, 56, 87];

// First test
$firstPosition = valuePosition($arr, 21);
echo $firstPosition;
echo "\n";

// Second test
$secondPosition = valuePosition($arr, 22);
echo $secondPosition;
echo "\n";

// Third test
$thirdPosition = valuePosition($arr, 4);
echo $thirdPosition;
echo "\n";
