<?php

namespace Eurecab\Finder\Algorithm;

final class UsersDifference
{
    /** @var User|null */
    private $youngest = null;

    /** @var User|null */
    private $oldest = null;

    /** @var int */
    private $differenceTime = 0;

    /**
     * UsersDifference constructor.
     * @param User|null $firstUser
     * @param User|null $secondUser
     */
    public function __construct($firstUser = null, $secondUser = null)
    {
        if ($firstUser !== null && $secondUser !== null) {
            if ($firstUser->getBirthDate() < $secondUser->getBirthDate()) {
                $this->setYoungest($firstUser);
                $this->setOldest($secondUser);
            } else {
                $this->setYoungest($secondUser);
                $this->setOldest($firstUser);
            }

            $this->setDifferenceTime(
                $this->getOldest()->getBirthDate()->getTimestamp()
                - $this->getYoungest()->getBirthDate()->getTimestamp()
            );
        }
    }

    /**
     * @return User|null
     */
    public function getYoungest(): ?User
    {
        return $this->youngest;
    }

    /**
     * @param User|null $youngest
     */
    public function setYoungest(?User $youngest): void
    {
        $this->youngest = $youngest;
    }

    /**
     * @return User|null
     */
    public function getOldest(): ?User
    {
        return $this->oldest;
    }

    /**
     * @param User|null $oldest
     */
    public function setOldest(?User $oldest): void
    {
        $this->oldest = $oldest;
    }

    /**
     * @return int
     */
    public function getDifferenceTime(): int
    {
        return $this->differenceTime;
    }

    /**
     * @param int $differenceTime
     */
    public function setDifferenceTime(int $differenceTime): void
    {
        $this->differenceTime = $differenceTime;
    }

}
